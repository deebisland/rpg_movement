var component;
var customRectangle;

function createCustomRectangle(x, y, z, color, width, height) {
    if (x === undefined) {
        x = 0
    }
    if (y === undefined) {
        y = 0
    }
    if (z === undefined) {
        z = 0
    }
    if (color === undefined) {
        color = "blue"
    }
    if (width === undefined) {
        width = 5
    }
    if (height === undefined) {
        height = 10
    }

    component = Qt.createComponent("CustomRectangle.qml")
    customRectangle = component.createObject(floor, {"x" : x, "y" : y, "z" : z, "color" : color, "width" : width, "height" : height})

    return customRectangle
}
