import QtQuick 2.5
import QtQml 2.2

// ------------------------------------------------------------------
// Use virtualJoyStickSpeed value to calculate speed. Value is given
// in multiples of 10. Value accelerates over 110 milliseconds.
// ------------------------------------------------------------------
// This file must be used in another project. Otherwise, a window
// encapsulation around the Rectangle below is requried.
// ------------------------------------------------------------------

Rectangle {
    property var virtualJoyStickGrid: []
    property int virtualJoyStickGridSelector: 0
    property int virtualJoyStickSpeed: innerCircle.x + innerCircle.width * 0.5 - outerCircle.width * 0.5 // even if speed is not being used (in GameLogic.qml), this tells left or right, -ve is left, +ve is right
    property int virtualJoyStickSpeedMax: outerCircle.width - 10 - outerCircle.width * 0.5 // (the last grid value) - outerCircle.width * 0.5
    property bool virtualJoyStickIsMovingUp: setVirtualJoyStickIsMovingUp()
    property bool virtualJoyStickIsMovingDown: setVirtualJoyStickIsMovingDown()
//    property var directionArrowCoordinateX: [0, 0.5, 1, 0.5, 0, -0.5, -1, -0.5]
//    property var directionArrowCoordinateY: [0, 0.5, 1, 1.5, 2, 1.5, 1, 0.5]

    function setInnerCircleX() {
        if (virtualJoyStickTouchPoint.x > hitboxOuterCircle.width * 0.55) {
            for (var a = 4; a != virtualJoyStickGrid.length; a++) {
                if (virtualJoyStickTouchPoint.x - outerCircle.x > virtualJoyStickGrid[a]) {
                    virtualJoyStickGridSelector = virtualJoyStickGrid[a]
                }
            }
            innerCircle.state = "rightOrLeft"
        }
        else if (virtualJoyStickTouchPoint.x < hitboxOuterCircle.width * 0.45){
            for (a = virtualJoyStickGrid.length; a >= 0; a--) {
                if (virtualJoyStickTouchPoint.x - outerCircle.x < virtualJoyStickGrid[a]) {
                    virtualJoyStickGridSelector = virtualJoyStickGrid[a]
                }
            }
            innerCircle.state = "rightOrLeft"
        }
        else {
            innerCircle.state = "middle"
        }
    }

    function setInnerCircleMiddle() {
        innerCircle.state = "middle"
    }

    function setVirtualJoyStickIsMovingUp() {
        return virtualJoyStickTouchPoint.y < outerCircle.height * 0.3 + outerCircle.y ? true : false
    }

    function setVirtualJoyStickIsMovingDown() {
        return virtualJoyStickTouchPoint.y > outerCircle.height * 0.7 + outerCircle.y ? true : false
    }

    id: hitboxOuterCircle
    color: "transparent"
    MultiPointTouchArea {
        focus: true
        anchors.fill: parent
        touchPoints:
            TouchPoint {
            id: virtualJoyStickTouchPoint
            onXChanged: {
                setInnerCircleX()
            }
            onYChanged: {
                virtualJoyStickIsMovingUp = setVirtualJoyStickIsMovingUp()
                virtualJoyStickIsMovingDown = setVirtualJoyStickIsMovingDown()
            }
            onPressedChanged: {
                if (virtualJoyStickTouchPoint.pressed == true) {
                    setInnerCircleX()
                    virtualJoyStickIsMovingUp = setVirtualJoyStickIsMovingUp()
                    virtualJoyStickIsMovingDown = setVirtualJoyStickIsMovingDown()
                }
                else {
                    innerCircle.state = "middle"
                    virtualJoyStickIsMovingUp = false
                    virtualJoyStickIsMovingDown = false
                }
            }
        }
    }

    Rectangle {
        id: outerCircle
        anchors.centerIn: parent
        width: hitboxOuterCircle.width * 0.5
        height: width
        radius: width * 0.5
        border.width: 3
        Component.onCompleted: {
            for (var a = 10; a != outerCircle.width; a += 10) { // TODO increments of 5 can also be used!, then allows the KIND/GENTLE manipulation of the innerCircle
                virtualJoyStickGrid.push(a)
            }
        }

        Rectangle {
            id: innerCircle
            width: outerCircle.width * 0.4
            height: width
            radius: width * 0.5
            border.width: 3
            states: [
                State {
                    name: "rightOrLeft"
                    PropertyChanges {
                        target: innerCircle
                        x: virtualJoyStickGridSelector - innerCircle.width * 0.5
                    }
                },
                State {
                    name: "middle"
                    PropertyChanges {
                        target: innerCircle
                        x: outerCircle.width * 0.5 - innerCircle.width * 0.5
                    }
                }
            ]
            Behavior on x {
                NumberAnimation {
                    duration: 110
                }
            }
            Component.onCompleted: {
                innerCircle.x = outerCircle.width * 0.5 - innerCircle.width * 0.5
                innerCircle.y = outerCircle.height * 0.5 - innerCircle.height * 0.5
                virtualJoyStickIsMovingUp = false
                virtualJoyStickIsMovingDown = false
            }
        }

//        Repeater {
//            model: 8
//            Image {
////                id: directionArrowNorth
//                source: "qrc:/Images/Game/arrow.png"
//                x: outerCircle.width * 0.5 - width * 0.5 + directionArrowCoordinateX[index] * innerCircle.width
//                y: outerCircle.height * 0.1 + directionArrowCoordinateY[index] * innerCircle.height * 0.5
//                rotation: 45 * index
//                width: outerCircle.width * 0.2
//                opacity: 0.4
//            }
//        }

        Rectangle {
            id: jumpLine
            width: outerCircle.width
            height: outerCircle.height * 0.01
            y: outerCircle.height * 0.3
            color: "black"
        }

        Rectangle {
            id: downLine
            width: outerCircle.width
            height: outerCircle.height * 0.01
            y: outerCircle.height * 0.7
            color: "black"
        }
    }
}
