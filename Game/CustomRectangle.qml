import QtQuick 2.0

Rectangle {
    property bool isMovingLeft: false
    property int distanceTravelled: 0

    Behavior on x {
        NumberAnimation {
            duration: 17
        }
    }
}
