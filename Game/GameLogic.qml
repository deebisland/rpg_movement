import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import "CreateCustomRectangle.js" as CreateCustomRectangleFactory

Rectangle {
    id: gameRoot
    visible: true
    width: Screen.width
    height: Screen.height
    color: "#eeffff"

    property bool isLastMovementLeft: false
    property bool isMovingLeft: virtualJoyStick.virtualJoyStickSpeed < 0 ? true : false
    property bool isMovingRight: virtualJoyStick.virtualJoyStickSpeed > 0 ? true : false
    property bool isMovingUp: false
    property bool isMovingDown: false
    property bool isMovingDownForced: false
    property bool isOnGround: false
    property int distanceMovedUp: 0
    property int distanceMovedUpMax: 40
    property int gravityMultiplier: 1
    property int jumpMultiplier: 1
    property int speedMultiplier: 1

    property int yLand: 0
    property int xLastCheckPoint: 200
    property int xLeftBarrier: 10

    property int wObstacle: Screen.width * 0.005
    property int hObstacle: Screen.height * 0.01
    property int wPlayer: Screen.width * 0.005
    property int hPlayer: Screen.height * 0.02

    property var obstacleArray: []
    property var bulletArray: []
    property var enemyArray: []
    property var landArray: []
    property var platformArray: []

    property int obstaclesDestroyed: 0
    property int enemiesKilled: 0
    property int enemyCharger: 0
    property int enemyChargerMax: 1700
    property int bulletsCreated: 0
    property int bulletsCreatedMax: 20
    property int bulletCharger: 0
    property int bulletChargerMax: 700

    property int hearts: 3
    property int timeRemaining: 120
    property bool isGamePaused: false
    property int scoreTotal: timeRemaining * 10 + enemiesKilled * 30 + hearts * 70 - bulletsCreated * 10

    onScoreTotalChanged: {
        if (scoreTotal <= 0) {
            console.log("invoke LOSE! by score!")
        }
    }

    Component.onCompleted: {
        startGame()
    }

    function startGame() {
        startTimers(false)
        for (var b = 0; b != enemyArray.length; b++) {
            enemyArray[b].visible = false
        }
        for (b = 0; b != bulletArray.length; b++) {
            bulletArray[b].visible = false
        }
        for (b = 0; b != landArray.length; b++) {
            landArray[b].visible = false
        }
        for (b = 0; b != obstacleArray.length; b++) {
            obstacleArray[b].visible = false
        }
        enemyArray = enemyArray.filter(isVisible)
        bulletArray = bulletArray.filter(isVisible)
        landArray = landArray.filter(isVisible)
        obstacleArray = obstacleArray.filter(isVisible)
        xLeftBarrier = 10
        bulletsCreated = 0
        virtualJoyStick.setInnerCircleMiddle()
        isLastMovementLeft = false
        isMovingDownForced = false
        player.rotation = 0
        distanceMovedUp = 0
        createLand()
        moveToLastCheckPoint()
        tmrResumeGame.running = true
    }

    function startTimers(option) {
        if (option === undefined) {
            option = true
        }
        isGamePaused = !option
        tmrDeleteObjects.running = option
        tmrMoveObjects.running = option
        tmrScoreDecreaser.running = option
    }

    function pauseGame(option) {
        if (option === undefined) {
            option = true
        }
        option = !option
        startTimers(option)
        if (isGamePaused === true) {
            pauseGameDialog.visible = true
        }
    }

    function createLand() {
        for (var a = 0; a != 5; a++) {
            obstacleArray.push(CreateCustomRectangleFactory.createCustomRectangle(100 + a * 80, -hObstacle, 0, "blue", wObstacle, hObstacle)) // root.height * (1 - floor.height) - CustomRectangle.height // this is old, new is -height
        }
        for (a = 0; a != 5; a++) {
            obstacleArray.push(CreateCustomRectangleFactory.createCustomRectangle(505 + a * 80, -10, 0, "darkBlue"))
        }

        for (a = 1; a != 6; a++) {
            // odd objects can move through left but not right, vice versa, is it because the +2 movement or some weird shit?
            // read condition below
            //  condition: x + width = odd, x = odd
            if (a % 2 === 1) {
                landArray.push(CreateCustomRectangleFactory.createCustomRectangle(930 + a * 15, -15 * a, 0, "black", 16, 15 * a))
            }
            else {
                landArray.push(CreateCustomRectangleFactory.createCustomRectangle(929 + a * 15, -15 * a, 0, "black", 16, 15 * a))
            }
        }

        for (a = 6; a != 1; a--) {
            // condition: x + width = odd, x = odd
            if (a % 2 === 1) {
                landArray.push(CreateCustomRectangleFactory.createCustomRectangle(2030 + a * 15, -15 * a, 0, "black", 16, 15 * a))
            }
            else {
                landArray.push(CreateCustomRectangleFactory.createCustomRectangle(2029 + a * 15, -15 * a, 0, "black", 16, 15 * a))
            }
        }

        for (a = 6; a != 1; a--) {
            // condition: x + width = odd, x = odd
            if (a % 2 === 1) {
                landArray.push(CreateCustomRectangleFactory.createCustomRectangle(4030 + a * 15, -15 * a, 0, "black", 16, 15 * a))
            }
            else {
                landArray.push(CreateCustomRectangleFactory.createCustomRectangle(4029 + a * 15, -15 * a, 0, "black", 16, 15 * a))
            }
        }

        platformArray.push(CreateCustomRectangleFactory.createCustomRectangle(3000, -20, 0, "brown", 20, 6))
        platformArray.push(CreateCustomRectangleFactory.createCustomRectangle(2960, -42, 0, "brown", 20, 6))
        platformArray.push(CreateCustomRectangleFactory.createCustomRectangle(2920, -65, 0, "brown", 20, 6))
    }

    function moveToLastCheckPoint() {
        player.x = xLastCheckPoint
        player.y = -hPlayer
    }

    function moveScene() {
        scene.contentX = player.x - Screen.width * 0.4 // TODO allow buffer region
    }

    function isVisible(elem) {
        return elem.visible === true
    }

    function isPlayerObstacleColliding() { // -- Later! ?? have all 3 functions in 1? 1 function for player obstacle, player enemy, bullet enemy? player end game? 3/4 functions in 1? land?
        for (var a = 0; a != obstacleArray.length; a++) {
            if ((player.x < obstacleArray[a].x + obstacleArray[a].width) && (player.x + player.width > obstacleArray[a].x) && (player.y < obstacleArray[a].y + obstacleArray[a].height) && (player.y + player.height > obstacleArray[a].y)) {
                startGame()
                return
            }
        }
    }

    function createBullet(visibleInput) {
        if (bulletCharger === bulletChargerMax) {
            bulletsCreated += 1 // -- Later! bullets cost money?
            bulletArray.push(CreateCustomRectangleFactory.createCustomRectangle(isLastMovementLeft === false ? player.x + player.width : player.x, player.y + player.height * 0.5 - 5 * 0.5, 1, "darkBrown", 10, 5)) // the -ve * 0.5 in y parameter is the height of the bullet
            bulletArray[bulletArray.length - 1].isMovingLeft = isLastMovementLeft
            if (visibleInput !== undefined) {
                bulletArray[bulletArray.length - 1].color = "#00000000"
            }
            bulletCharger = 0
        }
    }

    function isBulletColliding() {
        if (bulletArray.length === 0) {
            return
        }
        var a
        for (var b = bulletArray.length - 1; b >= 0; --b) {
            if (Qt.colorEqual(bulletArray[b].color, "#00000000")) {
                bulletArray[b].distanceTravelled += 3
                if (bulletArray[b].distanceTravelled === 90) { // todo this value needs to be relative? and global!
                    bulletArray[b].visible = false
                    bulletArray = bulletArray.filter(isVisible)
                    continue
                }
            }
            for (a = enemyArray.length - 1; a >= 0; --a) {
                if ((bulletArray[b].x < enemyArray[a].x + enemyArray[a].width) && (bulletArray[b].x + bulletArray[b].width > enemyArray[a].x) && (bulletArray[b].y < enemyArray[a].y + enemyArray[a].height) && (bulletArray[b].y + bulletArray[b].height > enemyArray[a].y)) {
                    if (!Qt.colorEqual(bulletArray[b].color, "#00000000")) { // todo run profile check on this!
                        bulletArray[b].visible = false
                        bulletArray = bulletArray.filter(isVisible)
                    }
                    else {
                        // todo damage the sword!
                    }
                    enemyArray[a].visible = false
                    ++enemiesKilled
                    enemyArray = enemyArray.filter(isVisible)
                    return
                }
            }
            for (a = obstacleArray.length - 1; a >= 0; --a) {
                if ((bulletArray[b].x < obstacleArray[a].x + obstacleArray[a].width) && (bulletArray[b].x + bulletArray[b].width > obstacleArray[a].x) && (bulletArray[b].y < obstacleArray[a].y + obstacleArray[a].height) && (bulletArray[b].y + bulletArray[b].height > obstacleArray[a].y)) {
                    if (!Qt.colorEqual(bulletArray[b].color, "#00000000"))  {
                        bulletArray[b].visible = false
                        bulletArray = bulletArray.filter(isVisible)
                    }
                    else {
                        // todo damage the sword!
                    }
                    obstacleArray[a].visible = false
                    ++obstaclesDestroyed
                    obstacleArray = obstacleArray.filter(isVisible)
                    return
                }
            }
        }
    }

    function createEnemy() {
        // todo + 600 should become relative
        // todo height, width should be relative!
        for (var a = 0; a !== enemyArray.length; ++a) {
            if (enemyArray[a].x > player.x + 580 && enemyArray[a].x < player.x + 620) {
                return
            }
        }
        enemyArray.push(CreateCustomRectangleFactory.createCustomRectangle(player.x + 600, -10, 1, "red"))
    }

    function isPlayerEnemyColliding() {
        for (var a = 0; a != enemyArray.length; a++) {
            if ((player.x < enemyArray[a].x + enemyArray[a].width) && (player.x + player.width > enemyArray[a].x) && (player.y < enemyArray[a].y + enemyArray[a].height) && (player.y + player.height > enemyArray[a].y)) {
                hearts -= 1 // todo if hearts == 0 invoke lose!
                startGame()
                return 1
            }
        }
    }

    function isPlayerLandColliding() {
        for (var a = 0; a != landArray.length; a++) {
            if ((player.x >= landArray[a].x && player.x <= landArray[a].x + landArray[a].width) || (player.x + player.width >= landArray[a].x && player.x + player.width <= landArray[a].x + landArray[a].width)) {
                if (player.y + player.height >= landArray[a].y) {
                    yLand = landArray[a].y
                    return 1
                }
            }
        }
    }

    function isPlayerLandColliding2() {
        for (var a = 0; a != landArray.length; a++) {
            if (player.y + player.height > landArray[a].y) {
                if ((player.x + player.width < landArray[a].x) && (player.x + player.width + 2 > landArray[a].x)) {
                    return 1
                }
                if ((player.x > landArray[a].x + landArray[a].width) && (player.x - 2 < landArray[a].x + landArray[a].width)) {
                    return 2
                }
            }
        }
    }

    function isPlayerPlatformColliding() {
        for (var a = 0; a != platformArray.length; a++) {
            if ((player.x >= platformArray[a].x && player.x <= platformArray[a].x + platformArray[a].width) || (player.x + player.width >= platformArray[a].x && player.x + player.width <= platformArray[a].x + platformArray[a].width)) {
                if (player.y + player.height + 2 >= platformArray[a].y && player.y + player.height <= platformArray[a].y && isMovingDown === true) {
                    yLand = platformArray[a].y
                    return 1
                }
                if (player.y - 2 <= platformArray[a].y + platformArray[a].height && player.y >= platformArray[a].y + platformArray[a].height && isMovingUp === true) {
                    return 2
                }
            }
        }
    }

    function isPlayerPlatformColliding2() {
        for (var a = 0; a != platformArray.length; a++) {
            if ((player.y >= platformArray[a].y && player.y <= platformArray[a].y + platformArray[a].height) || (player.y + player.height >= platformArray[a].y && player.y + player.height <= platformArray[a].y + platformArray[a].height)) {
                if (player.x + player.width < platformArray[a].x && player.x + player.width > platformArray[a].x && isMovingRight === true) {
                    return 1
                }
                if (player.x > platformArray[a].x + platformArray[a].width && player.x - 2 < platformArray[a].x + platformArray[a].width && isMovingLeft === true) {
                    return 2
                }
            }
        }
    }

    Timer {
        id: tmrResumeGame
        interval: 1000 // TODO change animation 2..1..
        running: false
        repeat: false
        onTriggered: {
            startTimers()
        }
    }

    Timer {
        id: tmrDeleteObjects
        interval: 5000
        running: false
        repeat: true
        onTriggered: { // TODO change these numbers to a percentage-based value system
            for (var b = 0; b != enemyArray.length; b++) {
                if (enemyArray[b].x - player.x < -500) {
                    enemyArray[b].visible = false
                }
            }
            for (b = 0; b != bulletArray.length; b++) {
                if (bulletArray[b].x - player.x > 1500 || bulletArray[b].x - player.x < -500) {
                    bulletArray[b].visible = false
                }
            }
            for (b = 0; b != landArray.length; b++) {
                if (landArray[b].x - player.x < -500) {
                    landArray[b].visible = false
                }
            }
            for (b = 0; b != obstacleArray.length; b++) {
                if (obstacleArray[b].x - player.x < -500) {
                    obstacleArray[b].visible = false
                }
            }
            enemyArray = enemyArray.filter(isVisible)
            bulletArray = bulletArray.filter(isVisible)
            landArray = landArray.filter(isVisible)
            obstacleArray = obstacleArray.filter(isVisible)
            if (xLeftBarrier < player.x - Screen.width) {
                xLeftBarrier = player.x - Screen.width
            }
        }
    }

    Timer {
        id: tmrScoreDecreaser
        interval: 1000
        running: false // add pause/play into the game, add 3 second countdown! // -- Later!
        repeat: true
        onTriggered: {
            timeRemaining -= 1
        }
    }

    Timer {
        id: tmrMoveObjects
        interval: 17
        running: false
        repeat: true
        onTriggered: {
            moveScene()
            // begin bulletMovement
            for (var a = 0; a != bulletArray.length; a++) {
                if (bulletArray[a].isMovingLeft === false) {
                    bulletArray[a].x += 3
                }
                else {
                    bulletArray[a].x -= 3
                }
            }
            if (bulletCharger < bulletChargerMax) {
                bulletCharger += 20
            }
            // begin enemyCreator
            if (enemyCharger === enemyChargerMax && player.x < floor.width * 0.85) {
                createEnemy()
                enemyCharger = 0
            }
            else {
                enemyCharger += 20
            }
            // begin enemyMovement
            for (a = 0; a != enemyArray.length; a++) {
                enemyArray[a].x -= 2
            }
            // begin playerMovement
            if (virtualJoyStick.virtualJoyStickIsMovingUp === true && isMovingUp === false && isMovingDown === false) {
                isMovingUp = true
            }
            if (virtualJoyStick.virtualJoyStickIsMovingDown === true) {
                isMovingUp = false
                isMovingDown = true
                isMovingDownForced = true
            }
            if (isMovingLeft === true) {
                isLastMovementLeft = true
                if (player.x - xLeftBarrier > 30) { // -- Later! delete objects left of this, make floor multiple floors? // stop creating enemies at 10% remaining! // make holes in the floor!!!!!
                    if (isPlayerLandColliding2() !== 2) {
                        player.x -= 2 * speedMultiplier
                        //                        player.x -= (2 - 0.5 * virtualJoyStick.virtualJoyStickSpeed / virtualJoyStick.virtualJoyStickSpeedMax) * speedMultiplier // virtualJoyStick.virtualJoyStickSpeed can be negative // no need for speed
                        isMovingDown = true
                    }
                }
            }
            else if (isMovingRight === true) {
                isLastMovementLeft = false
                if (isPlayerLandColliding2() !== 1) {
                    player.x += 2 * speedMultiplier
                    //                    player.x += (2 + 0.5 * virtualJoyStick.virtualJoyStickSpeed / virtualJoyStick.virtualJoyStickSpeedMax) * speedMultiplier
                    isMovingDown = true
                    if (player.x > finishFlag.x) {
                        txtWin.visible = true
                        txtWin.x = player.x
                        console.log("Invoke win!")
                    }
                }
            }
            if (isMovingUp === true) {
                player.rotation += 12
                distanceMovedUp += 2
                if (isPlayerPlatformColliding() !== 2) {
                    player.y -= 2 * jumpMultiplier
                }
                if (distanceMovedUp === distanceMovedUpMax) {
                    isMovingUp = false
                    isMovingDown = true
                    distanceMovedUp = 0
                }
            }
            else if (isMovingDown === true) {
                player.rotation += 12
                player.y += 2 * gravityMultiplier
                if (isMovingDownForced === true) {
                    player.y += 2
                }
                if ((player.y + player.height >= -2 && player.y + player.height <= 2) || (isPlayerLandColliding() === 1) || (isPlayerPlatformColliding() === 1)) {
                    isMovingDown = false
                    isMovingDownForced = false
                    player.rotation = 0
                    player.y = yLand - hPlayer
                    yLand = 0
                }
            }
            isBulletColliding()
            isPlayerEnemyColliding()
            isPlayerObstacleColliding()
        }
    }

    MessageDialog { // TODO change to custom dialog
        id: pauseGameDialog
        title: "Game is paused"
        text: "Do you want to continue game?"
        standardButtons: StandardButton.Yes
        onYes: {
            pauseGame(false)
        }
    }

    ScrollView {
        id: sclScene
        focus: true
        contentItem: scene
        width: Screen.width
        height: Screen.height
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOff

        Flickable {
            visible: true
            id: scene
            anchors.fill: parent
            contentHeight: Screen.height
            contentWidth: floor.width
            boundsBehavior: Flickable.StopAtBounds
            interactive: false
            Behavior on contentX {
                NumberAnimation {
                    duration: 500
                }
            }

            Rectangle {
                id: floor
                y: Screen.height * 0.6
                width: 5000 // CHANGE TO 10,000 for fun!
                height: Screen.height * 0.4
                color: "black"

                Rectangle {
                    id: player
                    width: wPlayer
                    height: hPlayer
                    x: 200
                    color: "darkGreen"
                    focus: true
                    Behavior on x {
                        NumberAnimation {
                            duration: 17
                        }
                    }
                    Behavior on y {
                        NumberAnimation {
                            duration: 17
                        }
                    }
                    Behavior on rotation {
                        NumberAnimation {
                            duration: 17
                        }
                    }
                    //                    Keys.onLeftPressed: {
                    //                        isMovingLeft = true
                    //                        isMovingRight = false
                    //                        isLastMovementLeft = true
                    //                    }
                    //                    Keys.onRightPressed: {
                    //                        isMovingLeft = false
                    //                        isMovingRight = true
                    //                        isLastMovementLeft = false
                    //                    }
                    //                    Keys.onUpPressed: {
                    //                        moveUp()
                    //                    }
                    //                    Keys.onSpacePressed: {
                    //                        moveUp()
                    //                    }
                    //                    Keys.onPressed: {
                    //                        if (event.key == 81) { // q
                    //                            createBullet()
                    //                        }
                    //                    }
                    //                    //                    Keys.onPressed: {
                    //                    //                        console.log(event.key)
                    //                    //                        // 81 = q
                    //                    //                        // 87 = w
                    //                    //                        // 69 = e
                    //                    //                    }
                    //                    function moveUp() {
                    //                        if (isMovingUp == true || isMovingDown == true) {
                    //                            return false
                    //                        }
                    //                        isMovingUp = true
                    //                    }
                    //                    Keys.onReleased: {
                    //                        if (event.key == Qt.Key_Left) {
                    //                            isMovingLeft = false
                    //                        }
                    //                        else if (event.key == Qt.Key_Right) {
                    //                            isMovingRight = false
                    //                        }
                    //                    }
                }

                Rectangle {
                    id: leftBarrier
                    x: xLeftBarrier
                    y: -30
                    width: 30
                    height: 30

                    Text {
                        anchors.fill: parent
                        text: "STOP!"
                    }
                }

                VirtualJoyStick {
                    id: virtualJoyStick
                    x: scene.contentX + scene.width - width * 1.2
                    width: height
                    height: Math.round(floor.height * 0.8 / 100) * 100 // value must be a multiple of 100
                    anchors.verticalCenter: floor.verticalCenter
                }

                Image {
                    id: playerCreateBullet
                    source: "qrc:/Images/Game/cross.png"
                    x: scene.contentX + width * 0.5
                    anchors.verticalCenter: floor.verticalCenter
                    width: ~~(virtualJoyStick.width * 0.5)
                    height: width

                    Text {
                        text: "Shoot"
                        font.pointSize: 14
                        color: "white"
                        anchors.centerIn: parent
                    }

                    MultiPointTouchArea {
                        anchors.fill: parent
                        touchPoints: [
                            TouchPoint {
                                id: playerCreateBulletTouchPoint
                                onPressedChanged: {
                                    if (playerCreateBulletTouchPoint.pressed) {
                                        createBullet()
                                    }
                                }
                            }
                        ]
                    }
                }

                Image {
                    id: playerCreateSword
                    source: "qrc:/Images/Game/rectangle.png"
                    x: scene.contentX + width * 1.4 // enough clearance for playerCreateBullet, maybe anchor off of that instead?
                    anchors.verticalCenter: floor.verticalCenter
                    anchors.verticalCenterOffset: height * 0.55
                    width: playerCreateBullet.width
                    height: width

                    Text {
                        text: "Sword"
                        font.pointSize: 14
                        color: "white"
                        anchors.centerIn: parent
                    }

                    MultiPointTouchArea {
                        anchors.fill: parent
                        touchPoints: [
                            TouchPoint {
                                id: playerCreateSwordTouchPoint
                                onPressedChanged: {
                                    if (playerCreateSwordTouchPoint.pressed) {
                                        createBullet(false) // make invis bullet?, destroy after travelling for X x-coordinates
                                    }
                                }
                            }
                        ]
                    }
                }

                Rectangle {
                    id: levelMap // later have map == all levels map? // later have some maps on hills going down and up?
                    anchors.bottom: floor.top
                    anchors.bottomMargin: Screen.height * 0.4
                    anchors.left: playerCreateBullet.right
                    anchors.leftMargin: Screen.width * 0.08
                    width: Screen.width * 0.3
                    height: 10
                    color: "#63e34a"
                    Behavior on x {
                        NumberAnimation {
                            duration: 500
                        }
                    }

                    Rectangle {
                        id: playerMap
                        x: (player.x - 200) / floor.width * levelMap.width
                        width: 10
                        height: 15
                        anchors.verticalCenter: parent.verticalCenter
                        color: "darkGreen"
                        Behavior on x {
                            NumberAnimation {
                                duration: 500
                            }
                        }
                    }

                    Image {
                        id: finishFlagMap
                        source: "../Images/Game/finishFlag.jpg"
                        width: 10
                        height: 10
                        anchors.right: levelMap.right
                        anchors.rightMargin: -3
                        anchors.bottom: levelMap.bottom
                    }
                }

                Rectangle {
                    id: bulletChargerBar
                    anchors.left: txtBulletCharger.right
                    anchors.leftMargin: 10
                    anchors.verticalCenter: txtBulletCharger.verticalCenter
                    width: Screen.width * 0.05
                    height: txtBulletCharger.height
                    color: "#ccf8ff"
                    Behavior on x {
                        NumberAnimation {
                            duration: 500
                        }
                    }

                    Rectangle {
                        id: bulletChargerBarFiller
                        anchors.left: bulletChargerBar.left
                        anchors.bottom: bulletChargerBar.bottom
                        width: bulletCharger / bulletChargerMax * bulletChargerBar.width
                        height: bulletChargerBar.height
                        color: "#4776b7"
                        Behavior on x {
                            NumberAnimation {
                                duration: 500
                            }
                        }
                    }
                }

                Text {
                    id: txtScoreTotal
                    text: "Score: " + scoreTotal
                    anchors.left: levelMap.left
                    anchors.top: levelMap.bottom
                    anchors.topMargin: Screen.height * 0.08
                    color: "green"
                    Behavior on x {
                        NumberAnimation {
                            duration: 500
                        }
                    }
                }

                Text {
                    id: txtHearts
                    text: "Hearts: " + hearts
                    anchors.left: txtScoreTotal.left
                    anchors.top: txtScoreTotal.bottom
                    anchors.topMargin: Screen.height * 0.01
                    color: "red"
                    Behavior on x {
                        NumberAnimation {
                            duration: 500
                        }
                    }
                }

                Text {
                    id: txtBulletCharger
                    text: "Bullet Charge: "
                    anchors.left: txtHearts.left
                    anchors.top: txtHearts.bottom
                    anchors.topMargin: Screen.height * 0.01
                    color: "blue"
                    Behavior on x {
                        NumberAnimation {
                            duration: 500
                        }
                    }
                }

                Text {
                    id: txtBulletAmmo
                    text: "Bullet Ammo: " + (bulletsCreatedMax - bulletsCreated)
                    anchors.left: txtBulletCharger.left
                    anchors.top: txtBulletCharger.bottom
                    anchors.topMargin: Screen.height * 0.01
                    color: "purple"
                    Behavior on x {
                        NumberAnimation {
                            duration: 500
                        }
                    }
                }
            }

            Image {
                id: finishFlag
                source: "../Images/Game/finishFlag.jpg"
                width: 30
                height: 50
                anchors.right: floor.right
                anchors.rightMargin: 5
                anchors.bottom: floor.top
            }

            Text {
                id: txtWin
                text: "You Win!\nScore: " + scoreTotal
                visible: false
            }
        }
    }
}
