TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ../../../templates/gradle/wrapper/gradle-wrapper.jar \
    ../../../templates/gradlew \
    ../../../templates/res/values/libs.xml \
    ../../../templates/build.gradle \
    ../../../templates/gradle/wrapper/gradle-wrapper.properties \
    ../../../templates/gradlew.bat \
    ../../../templates/AndroidManifest.xml \
    ../../../templates/res/values/libs.xml \
    ../../../templates/build.gradle

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/../../../templates

HEADERS +=
