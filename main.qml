import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import "Worlds/TestWorld"

Window {
    id: root
    visible: true
    width: Screen.width
    height: Screen.height
    color: "#eeffff"

    function showMainMenu() { // todo this is not referenced anywhere
//        console.log("showmainMenu called")// dbg
        loader.source = ""
        dbgRec1.visible = true
    }

    MessageDialog {
        id: exitDialog
        title: "Confirm Exit"
        text: "Are you sure you want to exit?"
        standardButtons: StandardButton.Yes | StandardButton.No
        onYes: {
            Qt.quit()
        }
    }

    Loader {
        id: loader
    }

    Rectangle {
        id: dbgRec1 // dbg
        width: 200
        height: 200
        color: "blue"
        focus: true

        MouseArea {
            anchors.fill: parent
            onClicked: {
                dbgRec1.visible = false
                loader.source = "Worlds/TestWorld/LevelOne.qml"
            }
        }

        Keys.onBackPressed: {
            if (loader.source == "") {
                exitDialog.visible = true
            }
            else {
                loader.item.pauseGame()
            }
        }
    }
}
